import { SET_ACTIVE_USER_ID } from "../constants/action-types";
export default function(state = null, { type, payload }) {
  switch (type) {
    case SET_ACTIVE_USER_ID:
      return payload;
    default:
      return state;
  }
}
